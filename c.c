/*

gcc c.c -Wall -g

./a.out



List code is based on the : 
Linked List Basics By Nick Parlante
Copyright © 1998-2001, Nick Parlante
cslibrary.stanford.edu/103/LinkedListBasics.pdf

-------------------------------

valgrind --tool=memcheck ./a.out
==4700== Memcheck, a memory error detector
==4700== Copyright (C) 2002-2012, and GNU GPL'd, by Julian Seward et al.
==4700== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==4700== Command: ./a.out
==4700== 
0 
1 
2 
3 
4 
5 
6 
==4700== 
==4700== HEAP SUMMARY:
==4700==     in use at exit: 0 bytes in 0 blocks
==4700==   total heap usage: 7 allocs, 7 frees, 112 bytes allocated
==4700== 
==4700== All heap blocks were freed -- no leaks are possible
==4700== 
==4700== For counts of detected and suppressed errors, rerun with: -v
==4700== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 2 from 2)


http://valgrind.org/docs/manual/mc-manual.html



*/

#include<stdio.h>
#include<stdlib.h>




struct node {
         int data ; 
         struct node *next ;
 };






/*
Takes a list and a data value.
Creates a new link with the given data and pushes
it onto the front of the list.
The list is not passed in by its head pointer.
Instead the list is passed in as a "reference" pointer
to the head pointer -- this allows us
to modify the caller's memory.

Push(&head, 1);  // note the &

*/
void Push(struct node** headRef, int data) {
struct node* newNode = malloc(sizeof(struct node));
newNode->data = data;
newNode->next = *headRef;
*headRef = newNode;
// The '*' to dereferences back to the real head
// ditto
}


/*

5) Build — Special Case + Tail Pointer
Consider the problem of building up the list {1, 2, 3, 4, 5} by appending the nodes to the
tail end. The difficulty is that the very first node must be added at the head pointer, but all
the other nodes are inserted after the last node using a tail pointer. The simplest way to
deal with both cases is to just have two separate cases in the code. Special case code first
adds the head node {1}. Then there is a separate loop that uses a tail pointer to add all the
other nodes. The tail pointer is kept pointing at the last node, and each new node is added
at tail->next. The only "problem" with this solution is that writing separate special
case code for the first node is a little unsatisfying. Nonetheless, this approach is a solid
one for production code — it is simple and runs fast.
*/



struct node* BuildList(int length) {
  struct node* head = NULL;
  struct node* tail;
  int i;

   // create sigle linked list 
  // Deal with the head node here, and set the tail pointer
  Push(&head, 0);
  tail = head;
  // Do all the other nodes using 'tail'
  for (i=1; i<length; i++) {
Push(&(tail->next), i); // add node at tail->next
tail = tail->next; // advance tail to point to last node
}
 
tail->next =head ; // close the list = now circular list 

return(head);
}



        


void DisplayList(struct node *current, int length)
{

        int i=0;
	while (i<length) {
                printf("%d \n", current->data);
                current = current->next;
                i++;
              }

        
}


void FreeList(struct node *list, int length)
{
   struct node  *current=NULL;

        int l=0;
	while (l<length) {
                current = list;
                list = list->next;
                free(current); current=NULL; 
                l++;
              }

        
}


int main()

{
 
 int length=7;
 struct node  *list=NULL;
 
 
  
 list = BuildList(length);
 DisplayList(list,length );
 FreeList(list,length );

return 0; 
}
